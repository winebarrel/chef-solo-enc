$: << File.expand_path(File.dirname(__FILE__) + '/lib')
require 'chef-solo-enc/constants'

Gem::Specification.new do |spec|
  spec.name              = 'chef-solo-enc'
  spec.version           = ChefSoloENC::VERSION
  spec.summary           = 'External Node Classifiers for chef-solo.'
  spec.require_paths     = %w(lib)
  spec.files             = %w(README) + Dir.glob('bin/**/*') + Dir.glob('lib/**/*')
  spec.author            = 'winebarrel'
  spec.email             = 'sgwr_dts@yahoo.co.jp'
  spec.homepage          = 'https://bitbucket.org/winebarrel/chef-solo-enc'
  spec.bindir            = 'bin'
  spec.license           = 'MIT'
  spec.executables << 'chef-solo'
  spec.add_dependency('chef', '~> 11.4.4')
end
