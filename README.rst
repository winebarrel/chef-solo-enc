chef-solo-enc
=============

Description
-----------

External Node Classifiers for chef-solo.

see http://docs.puppetlabs.com/guides/external_nodes.html


(!!notice!! chef-solo-enc overwrite chef-solo command)

Source Code
-----------

https://bitbucket.org/winebarrel/chef-solo-enc

Install
-------

gem install chef-solo-enc

Example
-------

External Node Classifiers
~~~~~~~~~~~~~~~~~~~~~~~~~

$CHEF_REPO/nodes/external_nodes

::

  #!/bin/bash
  echo '{"run_list":"recipe[any_recipe]"}' 


::

  shell> knife solo cook any_server -V
  Starting 'Run'
  Checking Chef version...
  ...
  [2013-07-15T15:11:21+00:00] INFO: External Node Classifiers Result is {"run_list"=>"recipe[any_recipet]"}
  Starting Chef Client, version 11.4.4
  [2013-07-15T15:11:21+00:00] INFO: *** Chef 11.4.4 ***
  ...
  [2013-07-15T15:11:22+00:00] INFO: Run List is [recipe[any_recipet]]
  ...

License
-------

This software is under "'I like puppet' License".

* Please remember Puppet once per year. 
* Please do not pitied people who use Puppet.
* Please use Puppet when you feel like it.

(`It's a joke.`_)

.. _`It's a joke.`: https://bitbucket.org/winebarrel/chef-solo-enc/src/9c2b46da2675e5fad43af5483f0a8d50f2a9a34b/chef-solo-enc.gemspec?at=master#cl-14
